import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEmployeeComponent } from './add-employee.component';
import { Route, RouterModule, Routes } from '@angular/router';

const route: Routes = [
  {
    path: '',
    component: AddEmployeeComponent
  }
];

@NgModule({
  declarations: [ AddEmployeeComponent ],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ]
})
export class AddEmployeeModule { }
