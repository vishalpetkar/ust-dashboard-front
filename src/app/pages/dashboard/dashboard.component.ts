import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { EmployeeState, GetEmployee, GetColumns } from 'src/app/store/employee.actions';
import { Employee } from 'src/app/models/employee.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  stateData;
  columnNames: string[] = [];
  employeesData: Employee[] = [];
  pages = 0;
  perPage = 5;
  startFrom = 0;

  @Select(EmployeeState) employees$: Observable<Employee[]>;

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.employees$.subscribe((data) => {
      this.stateData = data;
      this.employeesData = this.stateData.employeeData;
      this.columnNames = this.stateData.columns;
      const totalEmp = this.stateData.totalEmployeeCount;
      this.pages = Math.ceil(totalEmp / 5);
    });

    this.store.dispatch([
      new GetEmployee({perPage: this.perPage, startFrom: this.startFrom}), 
      new GetColumns()]);
  }

  titleCase(value: string, col: string): string {
    const arr = value.split('_');
    const newArr = arr.map(item => {
      return item.charAt(0).toUpperCase() + item.substr(1).toLowerCase();
    });
    return newArr.join(' ');
  }

  getValue(obj, property): string {
    return obj[property];
  }

  getNext(): void {
    // tslint:disable-next-line: radix
    this.startFrom += parseInt('' + this.perPage);
    this.store.dispatch([new GetEmployee({perPage: this.perPage, startFrom: this.startFrom})]);
  }

  getPrevious(): void {
    // tslint:disable-next-line: radix
    this.startFrom -= parseInt('' + this.perPage);
    this.store.dispatch([new GetEmployee({perPage: this.perPage, startFrom: this.startFrom})]);
  }
}
