import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

@NgModule({
  declarations: [ HeaderComponent, FooterComponent, SideMenuComponent, BreadcrumbComponent ],
  imports: [
    CommonModule
  ],
  exports: [ HeaderComponent, FooterComponent, SideMenuComponent, BreadcrumbComponent ]
})
export class ShareModule { }
