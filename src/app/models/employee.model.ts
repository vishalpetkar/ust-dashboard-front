export interface Employee {
    id: number;
    vertical: string;
    year: string;
    month: string;
    account: string;
    account_manager: string;
    pid: string;
    project_name: string;
    sow_start_date: string;
    sow_end_date: string;
    uid: string;
    emp_name: string;
    nationality_id: string;
    gender: string;
    date_of_joining: string;
    total_experience: string;
    primary_skill: string;
    secondary_skill: string;
    country_working: string;
    band_grade: string;
    currency: string;
    billing_rate: string;
    start_date: string;
    end_date: string;
    working_days: string;
    leave_days: string;
    net_days: string;
    overtime_rate: string;
    total_billing: string;
    billable: string;
    billed: string;
    reason: string;
    reason_for_unbilled: string;
    reason_for_billable: string;
    sst: string;
    po: string;
    po_balance: string;
    invoice_number: string;
    invoice_amount: string;
    upload_date: string;
    pay_date: string;
    piad: string;
    nationality: string;
    nationality_name: string;
    country_working_name: string;
    currency_name: string;
    primary_skill_title: string;
    secondary_skill_title: string;
    band_grade_title: string;
}

export interface EmployeeList {
    employees: Employee[];
    totalCount: number;
}