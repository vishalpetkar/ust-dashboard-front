import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShareModule } from 'src/app/shared/share.module';
import { EmployeeState } from './store/employee.actions';
import { NgxsModule, Store } from '@ngxs/store';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    NgxsModule.forRoot([
      EmployeeState
    ]),
    FormsModule,
    ShareModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [ Store ],
  bootstrap: [AppComponent]
})
export class AppModule { }
