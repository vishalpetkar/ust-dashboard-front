import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EmployeeList } from 'src/app/models/employee.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  get(id = 0, perRow = 5, startFrom = 0): Observable<EmployeeList> {
    return this.http.get<EmployeeList>('http://localhost/ust/ust-dashboard-api/api/employee/' + id + '/' + perRow + '/' + startFrom);
  }

  getColumns(): Observable<string[]> {
    return this.http.get<string[]>('http://localhost/ust/ust-dashboard-api/api/columns');
  }
}
