import { Injectable } from '@angular/core';
import { State, Action, StateContext } from '@ngxs/store';
import { Employee } from './../models/employee.model';
import { EmployeeService } from './../services/employee.service';

// naming your action metadata explicitly makes it easier to understand what the action
// is for and makes debugging easier.
export class GetEmployee {
  static readonly type = '[Employee] Get';
  constructor(public data: {perPage: number, startFrom: number}) {}
}

export class GetColumns {
    static readonly type = '[Employee] Columns';
}

export class AddEmployee {
    static readonly type = '[Employee] Add';
    constructor(public employee: Employee) {}
}

export interface EmployeeStateModel {
    employeeData: Employee[];
    columns: string[];
    totalEmployeeCount: number;
}

@State<EmployeeStateModel>({
  name: 'employee',
  defaults: {
    employeeData: [],
    columns: [],
    totalEmployeeCount: 0
  }
})
@Injectable()
export class EmployeeState {

  constructor(private employeeService: EmployeeService) {

  }

  @Action(AddEmployee)
  addEmployee(ctx: StateContext<EmployeeStateModel>, action: AddEmployee): void {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      employeeData: [...state.employeeData, action.employee]
    });
  }

  @Action(GetEmployee)
  getEmployee(ctx: StateContext<EmployeeStateModel>, action: GetEmployee): void {
    this.employeeService.get(0, action.data.perPage, action.data.startFrom).subscribe((data) => {
        console.log(action);
        const state = ctx.getState();
        ctx.setState({
            ...state,
            employeeData: data.employees,
            totalEmployeeCount: data.totalCount
        });
    });
  }

  @Action(GetColumns)
  getColumn(ctx: StateContext<EmployeeStateModel>): void {
    this.employeeService.getColumns().subscribe((data) => {
        const state = ctx.getState();
        ctx.setState({
            ...state,
            columns: data
        });
    });
  }
}
